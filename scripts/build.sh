#!/bin/bash

# Script to build the extension zip and install the package
#
# This Script is released under GPL v3 license
# Copyright (C) 2020-2022 Javad Rahmatzadeh

set -e

# cd to the repo root
cd "$( cd "$( dirname "$0" )" && pwd )/.."

echo "Packing extension..."
gnome-extensions pack src \
    --force \
    --podir="../po" \
    --extra-source="bin" \
    --extra-source="libs" \
    --extra-source="ui" \
    --extra-source="preferences" \
    --extra-source="icons" \
    --extra-source="../LICENSE" \
    --extra-source="../CHANGELOG.md"

echo "Packing Done!"

#export G_MESSAGES_DEBUG=all

while getopts irp flag; do
    case $flag in

        i)  gnome-extensions install --force \
            drive-activity-indicator@marcosdalvarez.org.shell-extension.zip && \
            echo "Extension is installed. Now restart the GNOME Shell." || \
            { echo "ERROR: Could not install the extension!"; exit 1; };;

        r)  clear; dbus-run-session -- gnome-shell --nested --wayland
            ;;
        p)  clear; gnome-extensions prefs drive-activity-indicator@marcosdalvarez.org; journalctl -f -o cat /usr/bin/gjs
            ;;
        *)  echo "ERROR: Invalid flag!"
            echo "Use '-i' to install the extension to your system."
            echo "Use '-r' to run a nested instance of GNOME Shell."
            echo "Use '-p' to run preference window."
            echo "To just build it, run the script without any flag."
            exit 1;;
    esac
done

