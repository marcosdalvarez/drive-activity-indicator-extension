# Changelog

<!-- 
added: New feature
fixed: Bug fix
changed: Feature change
deprecated: New deprecation
removed: Feature removal
security: Security fix
performance: Performance improvement
other: Other 
-->

## [Unreleased]
- Sorry for my English, I use a translator. :)

## [v1] - 2024-05-06
### Added
- Initial release.

## [v2] - 2024-05-10
### Added
- Added Italian language

## [v3] - 2024-08-02
### Added
- Added support to Gnome 47

## [v4] - 2024-08-02
### Added
- Added Portuguese (Portugal and Brazilian) language

## [v5] - 2024-12-22
### Added
- Added Tamil and Hungarian languages

## [v6] - 2025-02-02
# Added
- Gnome 48 support
