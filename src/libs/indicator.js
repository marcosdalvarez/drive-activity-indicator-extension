'use strict';

import GObject from 'gi://GObject';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';

export const DriveActivityIndicator = GObject.registerClass({
    GTypeName: 'DriveActivityIndicator',
}, class DriveActivityIndicator extends QuickSettings.SystemIndicator {
    constructor(extensionObject) {
        super(extensionObject);

        // Text decoder
        this._decoder = new TextDecoder('utf-8');

        // Settings
        this._settings = extensionObject.getSettings();
        this._settingsValues = {
            includeRemovables: this._settings.get_boolean('include-removable-drives'),
            updateInterval: this._settings.get_int('update-interval'),
            idleColor: this._settings.get_string('idle-color'),
            readColor: this._settings.get_string('read-color'),
            writeColor: this._settings.get_string('write-color'),
            readWriteColor: this._settings.get_string('read-write-color'),            
        };
        this._settings.connectObject(
            'changed::include-removable-drives', () => {
                this._settingsValues.includeRemovables = this._settings.get_boolean('include-removable-drives');
            },
            'changed::update-interval', () => {
                this._settingsValues.updateInterval = this._settings.get_int('update-interval');
                this._createLoop();
            },
            'changed::idle-color', () => {
                this._settingsValues.idleColor = this._settings.get_string('idle-color');
            },
            'changed::read-color', () => {
                this._settingsValues.readColor = this._settings.get_string('read-color');
            },
            'changed::write-color', () => {
                this._settingsValues.writeColor = this._settings.get_string('write-color');
            },
            'changed::read-write-color', () => {
                this._settingsValues.readWriteColor = this._settings.get_string('read-write-color');
            },
            this
        );

        // Statistics file
        this._statFile = Gio.File.new_for_path('/proc/diskstats');
        // Reading flag
        this._readingFlag = false;
        // Used to cancel read operations
        this._cancellable = new Gio.Cancellable();

        // Previous statistics
        this._previousDiskStats = "";

        // Previous status
        // r = read, w = write, rw = read and write , other = no activty
        this._previousStatus = "";

        // Indicator
        this._indicator = this._addIndicator();
        this._indicator.icon_name = 'drive-harddisk-symbolic';
        this._indicator.set_style(`color: ${this._settingsValues.idleColor}`);
        Main.panel.statusArea.quickSettings.addExternalIndicator(this);

        // Drives
        this._connectedDrives = [];
        this._volumeMonitor = Gio.VolumeMonitor.get();
        this._volumeMonitor.connectObject(
            'drive-connected', () => {
                this._updateDrives();
            },
            'drive-disconnected', () => {
                this._updateDrives();
            },
            this
        );
        this._updateDrives();

        this._createLoop();

        this.connect('destroy', () => {
            this._settings.disconnectObject(this);
            this._settings = null;
            this._volumeMonitor.disconnectObject(this);
            this._volumeMonitor = null;
            this._destroyLoop();
            this._cancellable.cancel();
            this._cancellable = null;
            this._connectedDrives = null;
            this._statFile = null;
            this._decoder = null;
        });
    }

    /**
     * Create the read loop
     */
    _createLoop() {
        this._destroyLoop();
        this._loopId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, this._settingsValues.updateInterval, () => {
            this._readDiskStats();
            return GLib.SOURCE_CONTINUE;
        });
    }

    /**
     * Destroy the read loop
     */
    _destroyLoop() {
        if (this._loopId) {
            GLib.Source.remove(this._loopId);
            this._loopId = null;
        }
    }

    /**
     * Read statistics file
     */
    _readDiskStats() {
        if (!this._readingFlag) {
            this._readingFlag = true;
            this._statFile.load_contents_async(this._cancellable, (file, result) => {
                try {
                    const [, contents, ] = file.load_contents_finish(result);
                    this._updateActivityStatus(this._decoder.decode(contents));
                } catch (e) {
                    if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED)) {
                        console.error(e, `Reading ${file.get_path()}`);
                    }
                }
                this._readingFlag = false;
            });
        } else {
            console.warn('Skip reading. Try increasing the interval.');
        }
    }

    /**
     * Update current status based on disk statistics
     * @param {String} diskStats Disk statistics (/proc/diskstats)
     */
    _updateActivityStatus(diskStats) {
        let currentStatus = "";
        if (diskStats != this._previousDiskStats) {
            this._connectedDrives.forEach(drive => {
                if (!drive.is_removable() || this._settingsValues.includeRemovables) {
                    const identifier = drive.get_identifier(Gio.DRIVE_IDENTIFIER_KIND_UNIX_DEVICE).split('/').pop();
                    const regex = new RegExp('(' + identifier + ' .*)','g');
                    const currentData = diskStats.match(regex)?.[0];
                    const lastData = this._previousDiskStats.match(regex)?.[0];
                    if (currentData != lastData) {
                        const currentFields = currentData.split(/[ ]+/g);
                        if (lastData) {
                            const lastFields = lastData.split(/[ ]+/g);
                            if (currentFields[3] != lastFields[3]) { // # of sectors read
                                currentStatus += "r";
                            }
                            if (currentFields[7] != lastFields[7]) { // # of sectors written
                                currentStatus += "w";
                            }
                        }
                        this._updateIcon(currentStatus);
                    }
                }
            });
            this._previousDiskStats = diskStats;
        } else {
            this._updateIcon(currentStatus)
        }
    }

    /**
     * Update current connected drives
     */
    _updateDrives() {
        this._connectedDrives = this._volumeMonitor.get_connected_drives();
        this._previousDiskStats = "";
    }

    /**
     * Updates the indicator icon based on activity status
     * @param {String} currenStatus Current status (r = read, w = write, rw = read and write , other = no activty)
     */
    _updateIcon(currenStatus) {
        if (currenStatus != this._previousStatus) {
            this._previousStatus = currenStatus;
            let color = this._settingsValues.idleColor;
            switch (this._previousStatus) {
                case "r":
                    color = this._settingsValues.readColor;
                    break;
                case "w":
                    color = this._settingsValues.writeColor;
                    break;
                case "rw":
                    color = this._settingsValues.readWriteColor;
                    break;
                default:
                    color = this._settingsValues.idleColor;
                    break;
            }
            this._indicator.set_style(`color: ${color}`);
        }
    }
});